package com.example.homework1;

import java.io.Serializable;

public class Product implements Serializable {
    private int img;
    private String detail;

    public Product() {
    }

    public Product(int img, String detail) {
        this.img = img;
        this.detail = detail;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "Product{" +
                "img=" + img +
                ", detail='" + detail + '\'' +
                '}';
    }
}
