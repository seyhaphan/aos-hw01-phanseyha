package com.example.homework1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


public class DetailActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView textView;
    private Button btnSend;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        imageView = findViewById(R.id.imageView);
        textView = findViewById(R.id.textView);
        btnSend = findViewById(R.id.btnSend);
        editText = findViewById(R.id.etMessage);

        Intent intent = getIntent();

        Product product = (Product) intent.getSerializableExtra("DATA");

        textView.setText(product.getDetail());
        imageView.setImageResource(product.getImg());

        btnSend.setOnClickListener((view) -> {

            String message =  editText.getText().toString();

            intent.putExtra("BACK_DATA", message);

            setResult(RESULT_OK, intent);

            finish();
        });

    }
}