package com.example.homework1;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE = 1;

    private ImageView imageView1;
    private ImageView imageView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView1 = (ImageView) findViewById(R.id.imageView1);
        imageView2 = (ImageView) findViewById(R.id.imageView2);

        Intent intent = new Intent(MainActivity.this,DetailActivity.class);

        imageView1.setOnClickListener((view) ->{

            intent.putExtra("DATA",(Product) new Product(R.drawable.img1,getString(R.string.detail_img1)));

            startActivityForResult(intent,REQUEST_CODE);
        });

        imageView2.setOnClickListener((view)->{

            intent.putExtra("DATA",(Product)  new Product(R.drawable.img2,getString(R.string.detail_img2)));

            startActivityForResult(intent,REQUEST_CODE);
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == REQUEST_CODE  && resultCode  == RESULT_OK) {

                String requiredValue = data.getStringExtra("BACK_DATA");
                Toast.makeText(this, requiredValue,
                        Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.toString(),
                    Toast.LENGTH_SHORT).show();
        }


    }
}